import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "hola Flutter";
String thaiGreeting = "สวัสดี Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = englishGreeting;
                    });
                  },
                  icon: Icon(Icons.refresh)),
              IconButton(onPressed: (){
                setState(() {
                  displayText = thaiGreeting;
                });
              }, icon: Icon(Icons.favorite)),
              IconButton(onPressed: (){
                setState(() {
                  displayText = spanishGreeting;
                });
              }, icon: Icon(Icons.gamepad)),

            ],
          ),
          body: Center(
            child: Text(displayText, style: TextStyle(fontSize: 25)),
          )),
    );
  }
}

// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//
//         home: Scaffold(
//           appBar:AppBar(
//             title:Text("Hello Flutter"),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(
//                   onPressed: (){},
//                   icon: Icon(Icons.refresh))
//             ],
//           ),
//           body: Center(
//             child: Text(
//                 "Hello Flutter",
//             style: TextStyle(fontSize: 25)
//             ),
//           )
//         ),
//     );
//   }
// }
